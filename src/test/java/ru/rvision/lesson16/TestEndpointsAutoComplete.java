package ru.rvision.lesson16;

import io.qameta.allure.Story;
import io.qameta.allure.testng.Tag;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.rvision.lesson16.model.News;
import ru.rvision.lesson16.model.Resource;
import ru.rvision.lesson16.model.Station;

import static org.hamcrest.Matchers.equalTo;

@Story("Проверка endpoints '/v2/auto-complete'")
public class TestEndpointsAutoComplete extends BaseTest {

    @Tag("auto-complete")
    @Test(description = "Проверка получения данных с рандомным значением города на русском языке")
    public void checkRandomCityValueInRussian() {
        RestAssured
                .given()
                .spec(randomCitySpec())
                .when()
                .get("/v2/auto-complete")
                .then()
                .spec(responseBaseSpec())
                .body("data.cities[0].country", equalTo("Russia"));
    }

    @Tag("auto-complete")
    @Test(description = "Проверка получения данных без авторизации")
    public void checkWithoutAuthorization() {
        RestAssured
                .given()
                .spec(notAuthGetSpec())
                .when()
                .get("/v2/auto-complete")
                .then()
                .statusCode(401);
    }

    @Tag("auto-complete")
    @Test(description = "Проверка, что в блоке News есть только элементы с type = 'news'")
    public void checkNewsBlockContainsTypeNews() {
        boolean actualResult = RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .get("/v2/auto-complete")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("data.news", News.class)
                .stream()
                .allMatch(news -> news.type.equals("news"));
        Assert.assertTrue(actualResult ,"В блоке News есть элементы c типом не новости!");
    }


    @Tag("auto-complete")
    @Test(description = "Проверить что в блоке Resourse нету пусты title")
    public void checkEmptyTitleResourceBlock() {
        boolean actualResult = RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .get("/v2/auto-complete")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("data.resources", Resource.class)
                .stream()
                .noneMatch(resource -> resource.title.isEmpty());
        Assert.assertTrue(actualResult);
    }

    @DataProvider(name = "stationNamesForMoscow")
    public Object[][] dataProvidersForStationNamesForMoscow() {
        return new Object[][]{
                {"Тимошкино", true},
                {"Sadovyy Proyezd", true},
                {"ЖК Садовые Кварталы", true},
                {"Skyfort", true},
                {"Error", false},
                {"", false}
        };
    }

    @Tag("auto-complete")
    @Test(description = "Проверка названий станций в блоке Station для города Москва",
            dataProvider = "stationNamesForMoscow")
     public void checkStationNamesForMoscow(String nameStation, boolean expectedResult) {
        boolean actualResult = RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .get("/v2/auto-complete")
                .then()
                .spec(responseWithoutLogSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("data.stations", Station.class)
                .stream()
                .map(Station::getName)
                .toList().contains(nameStation);

        Assert.assertEquals(actualResult, expectedResult);
    }
}



