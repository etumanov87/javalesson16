package ru.rvision.lesson16;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.qameta.allure.Story;
import io.qameta.allure.testng.Tag;
import io.restassured.RestAssured;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

@Story("Проверка endpoints '/places/v2/list'")
public class TestEndpointsPlace extends BaseTest {

    @Tag("places/v2/list")
    @Test(description = "Проверка на получение ответа с случайными значение ID City и Station полученный с помощью GET запроса")
    public void checkForResponseWithRandomIDCityAndStation() throws JsonProcessingException {

        RestAssured
                .given()
                .spec(randomCityAndStationPostSpec())
                .post("/places/v2/list")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @DataProvider(name = "idCityAndStationNames")
    public Object[][] dataProvidersForCorrectlyValuesForCityAndStation() {
        return new Object[][]{
                {"DN4mWpnyhqqjZvDzS", "Moscow", "tngJr3N6wq6J5gH6g", "Breeeth! Klinskaya 6"},
                {"NAWD4zgao7hzk4ZiH", "Barcelona", "qYNn2ZT8vHyLakg9T", "Grácia - Sant Gervasi"},
        };
    }

    @Tag("places/v2/list")
    @Test(description = "Проверка правильно переданных значений id для City и Station",
            dataProvider = "idCityAndStationNames")
    public void checkCorrectlyValuesForCityAndStation(String idCity, String actualCity, String idStation, String actualStation) throws JsonProcessingException {
        RestAssured
                .given()
                .spec(requestWithCityAndStationPostSpec(idCity, idStation))
                .post("/places/v2/list")
                .then()
                .spec(responseBaseSpec())
                .body("data[0].city", equalTo(actualCity))
                .body("data[1].name", equalTo(actualStation));
    }

    @Tag("places/v2/list")
    @Test(description = "Проверка POST запроса с пустыми значения City и Station в теле запроса")
    public void checkRespondWithEmptyCityAndStation() throws JsonProcessingException {
        RestAssured
                .given()
                .spec(requestWithCityAndStationPostSpec("", ""))
                .post("/places/v2/list")
                .then()
                .spec(responseBaseSpec())
                .body("status", equalTo("fail"))
                .body("data.message", equalTo("parameters_missing"));
    }
}
