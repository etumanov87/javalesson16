package ru.rvision.lesson16;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.Locale;
import java.util.Map;

import static ru.rvision.lesson16.CreateJSONFile.pojoToJsonString;
import static ru.rvision.lesson16.CreateJSONFile.pojoToJsonStringWithRandomData;

/**
 * https://rapidapi.com/apidojo/api/airvisual1
 */
public class BaseTest {

    static String baseURL = "https://airvisual1.p.rapidapi.com";
    static Faker fakerRu = new Faker(Locale.forLanguageTag("ru"));
    private static final Map<String, String> authorizationData =  Map.of(
            "X-RapidAPI-Key", "b72d3bef79msh5ff8165e4a5af2ap10d911jsndce2dbca934e",
            "X-RapidAPI-Host", "airvisual1.p.rapidapi.com"
    );



    private static final Map<String,String> paramsData = Map.of(
            "x-user-lang", "en-US",
            "x-user-timezone", "Europe/Moscow",
            "x-aqi-index", "us",
            "x-units-pressure", "mbar",
            "x-units-distance", "kilometer",
            "x-units-temperature", "celsius"
    );

    public static RequestSpecification notAuthGetSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .addQueryParam("q","moscow")
                .addParams(paramsData)
                .log(LogDetail.ALL)
                .build();
    }
    public static RequestSpecification baseGetSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .addQueryParam("q","moscow")
                .addParams(paramsData)
                .addHeaders(authorizationData)
                .log(LogDetail.ALL)
                .build();
    }

    public static RequestSpecification baseGetWithoutLogSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .addQueryParam("q","moscow")
                .addParams(paramsData)
                .addHeaders(authorizationData)
                .build();
    }

    public static RequestSpecification randomCityAndStationPostSpec() throws JsonProcessingException {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .setContentType(ContentType.JSON)
                .addHeaders(authorizationData)
                .setBody(pojoToJsonStringWithRandomData())
                .log(LogDetail.ALL)
                .build();
    }

    public static RequestSpecification requestWithCityAndStationPostSpec(String idCity, String idStation) throws JsonProcessingException {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .setContentType(ContentType.JSON)
                .addHeaders(authorizationData)
                .setBody(pojoToJsonString(idCity, idStation))
                .log(LogDetail.ALL)
                .build();
    }
    public static RequestSpecification randomCitySpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .addQueryParam("q", fakerRu.address().cityName().toLowerCase())
                .addParams(paramsData)
                .addHeaders(authorizationData)
                .log(LogDetail.ALL)
                .build();
    }

    public static ResponseSpecification responseBaseSpec() {
        return new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
    }

    public static ResponseSpecification responseWithoutLogSpec() {
        return new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
    }

}
