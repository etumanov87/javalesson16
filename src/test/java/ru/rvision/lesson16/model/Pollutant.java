package ru.rvision.lesson16.model;

import lombok.Data;

@Data
public class Pollutant{
    public double conc;
    public int aqius;
    public int aqicn;
    public String pollutant;
    public int isEstimated;
}
