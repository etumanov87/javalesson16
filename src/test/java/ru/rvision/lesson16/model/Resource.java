package ru.rvision.lesson16.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Resource {
    public String author;
    public String date;
    public String status;
    public String thumbnail;
    public String title;
    public String type;
    public String url;
}
