package ru.rvision.lesson16.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Station{
    public String id;
    public String name;
    public String city;
    public String state;
    public String country;
    public Location location;
    public CurrentMeasurement currentMeasurement;
    public ArrayList<SensorDefinition> sensorDefinitions;
    public String type;
}
