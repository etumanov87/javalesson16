package ru.rvision.lesson16.model;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CurrentMeasurement{
    public String ts;
    public int aqius;
    public int aqicn;
    public int isEstimated;
    public ArrayList<Pollutant> pollutants;
}
