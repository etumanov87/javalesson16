package ru.rvision.lesson16.model;

import lombok.Data;

@Data
public class Location{
    public double lon;
    public double lat;
}
