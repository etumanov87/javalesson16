package ru.rvision.lesson16.model;

import lombok.Data;

@Data
public class SensorDefinition{
    public String pollutant;
    public String unit;
    public String name;
}
