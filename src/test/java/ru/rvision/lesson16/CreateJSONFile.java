package ru.rvision.lesson16;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import ru.rvision.lesson16.model.City;
import ru.rvision.lesson16.model.PlaceData;
import ru.rvision.lesson16.model.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CreateJSONFile extends BaseTest {

    static ObjectMapper objectMapper = new ObjectMapper();
    static Random random = new Random();


    public static String pojoToJsonString(String idCites, String idStation) throws JsonProcessingException {
        List<PlaceData> placeDataList = new ArrayList<>();

        placeDataList.add(new PlaceData(idCites, "city"));
        placeDataList.add(new PlaceData(idStation, "station"));
        placeDataList.add(new PlaceData("5c652484eff5bf47a1cac359", "device"));

        return objectMapper.writeValueAsString(placeDataList);
    }

    public static String pojoToJsonStringWithRandomData() throws JsonProcessingException {

        return pojoToJsonString(listIdCites.get(random.nextInt(0, listIdCites.size() - 1)),
                listIdStations.get(random.nextInt(0, listIdStations.size() - 1)));
    }

    static List<String> listIdCites =
            RestAssured
                    .given()
                    .spec(baseGetWithoutLogSpec())
                    .when()
                    .get("/v2/auto-complete")
                    .then()
                    .spec(responseWithoutLogSpec())
                    .extract()
                    .body()
                    .jsonPath()
                    .getList("data.cities", City.class)
                    .stream()
                    .map(city -> city.id)
                    .toList();

    static List<String> listIdStations =
            RestAssured
                    .given()
                    .spec(baseGetWithoutLogSpec())
                    .when()
                    .get("/v2/auto-complete")
                    .then()
                    .spec(responseWithoutLogSpec())
                    .extract()
                    .body()
                    .jsonPath()
                    .getList("data.stations", Station.class)
                    .stream()
                    .map(station -> station.id)
                    .toList();
}
